
# coding: utf-8

# # Paytm Challenge - Weblog Challenge

from pathlib import Path
from datetime import timedelta
import os
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
plt.style.use("dark_background")

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error


DIR_PATH = os.getcwd()
PATH = DIR_PATH+ "\\data\\"
OUTPUT_PATH = DIR_PATH + "\\output\\"

write_data_flag = False

log_regex_pattern = r'(\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) "([^"]+)" "([^"]*)" (\S+) (\S+)'

columns = ("timestamp elb client:port backend:port request_processing_time  backend_processing_time response_processing_time elb_status_code backend_status_code received_bytes sent_bytes request user_agent ssl_cipher ssl_protocol".split())
df = pd.read_csv(PATH+"2015_07_22_mktplace_shop_web_log_sample.log",sep=" ",names=columns)


df["timestamp"] = pd.to_datetime(df["timestamp"], format="%Y-%m-%dT%H:%M:%S.%fZ")
df["client_ip"] = df["client:port"].str.split(":", expand=True)[0]
df["url"] = df["request"].str.split(" ", expand=True)[1]
data = df.sort_values(['client_ip', 'timestamp']).reset_index(drop=True)


# ### 1. Sessionize the web log by IP. Sessionize = aggregrate all page hits by visitor/IP during a session.

data["prev_timestamp"] = data[['client_ip', 'timestamp']].groupby('client_ip').transform(lambda x:x.shift(1))
inactive_window = timedelta(seconds=10*60)
data['next_session'] = ((data['timestamp'] - data['prev_timestamp']) >= inactive_window).astype(int)
data['session'] = data.groupby("client_ip")['next_session'].cumsum()
data['session_id'] = data["client_ip"].str.cat(data["session"].astype(str), sep='_')
cols_order = ['timestamp', 'elb', 'client:port', 'client_ip', 'session', 'session_id',
              'backend:port', 'request_processing_time', 'backend_processing_time',
              'response_processing_time', 'elb_status_code', 'backend_status_code',
              'received_bytes', 'sent_bytes', 'request', 'url', 'user_agent', 'ssl_cipher', 'ssl_protocol']
if write_data_flag:
    data.to_csv(OUTPUT_PATH+"1.Sessionized Data.csv")
temp =data[cols_order].head(5)


# Drop user sessions with only first session and one record in the data
user_counts = data["session_id"].value_counts().reset_index().rename(columns={'index': 'session_id', 'session_id': 'count'})
single_record_sessions = user_counts.loc[user_counts["count"] == 1, "session_id"].values
data = data[~((data["session_id"].isin(single_record_sessions)) & (data["session"] == 0))]


# ### 2. Determine the average session time


session_length_data = (data[['session_id', 'timestamp']].groupby('session_id').agg({'timestamp': [('session_length', lambda x: np.ptp(x).seconds)]}))
session_length_data.columns = session_length_data.columns.droplevel()
session_length_data.reset_index(drop=False, inplace=True)

session_length_data['session_length'] = session_length_data['session_length'].astype(str)
session_length_data['session_length'] = session_length_data['session_length'].str.split(".", expand=True)[1]
session_length_data['session_length'] = session_length_data['session_length'].astype(int)

session_length_data["client_ip"], session_length_data["session"] = session_length_data["session_id"].str.split("_").str
session_length_data["session"] = session_length_data["session"].astype(int)
print("Average session time across all users:" + str(session_length_data['session_length'].mean()) +  " seconds")


### 3. Determine unique URL visits per session. To clarify, count a hit to a unique URL only once per session.

url_count_data = (data[['session_id', 'url']].groupby('session_id').agg({'url': [('unique_url_count', lambda x: x.nunique())]}))
url_count_data.columns = url_count_data.columns.droplevel()
url_count_data.reset_index(drop=False, inplace=True)
url_count_data.to_csv(OUTPUT_PATH+"3.unique URL visits per session.csv")
url_count_data.head(10)


### 4. Find the most engaged users, ie the IPs with the longest session times

ip_data = (session_length_data.groupby('client_ip')['session_length'].agg([('average_session_length', np.mean),
                                                                           ('number_of_sessions', 'count')])
                                                                     .reset_index()
                                                                     .sort_values('average_session_length', ascending=False))
if write_data_flag:
    ip_data.to_csv(OUTPUT_PATH+"4.most_engazed_users.csv")


# ## Part-2

### 1. Predict the expected load (requests/second) in the next minute

load_data = data.set_index("timestamp").resample("1Min").agg({'elb': [('Load', lambda x: np.count_nonzero(x)/60)],
                                                        'sent_bytes': [('sent_bytes', lambda x: x.astype(float).sum())],
                                                        'received_bytes': [('received_bytes', lambda x: x.astype(float).sum())],
                                                        'response_processing_time': [('response_processing_time', lambda x: pd.Series(x).replace({-1: 0}).astype(float).sum())],
                                                        'session_id': [('sessions_active', lambda x: x.nunique())]}).reset_index(col_fill="Time")
load_data.columns = load_data.columns.droplevel()

lag_cols = ["sent_bytes", "received_bytes", "response_processing_time", "sessions_active"]
tmp = load_data[lag_cols+["Load"]].shift(1)
tmp.columns = ["Prev_" + col for col in lag_cols+["Load"]]
load_data = pd.concat((load_data.drop(lag_cols, axis=1), tmp), axis=1).fillna(0)
load_data["Minute"] = load_data["Time"].dt.minute
cols_for_model = np.setdiff1d(load_data.columns, ["Time", "Load"])
load_data.head()


### Random forest model on lag data for predicting expected load
# build:val:test = 60:20:20 Time based split
train, test, y_train, y_test = train_test_split(load_data, load_data["Load"], test_size=0.2, random_state=2018, shuffle=False)

build, val, y_build, y_val = train_test_split(train, y_train, test_size=0.25, random_state=2018, shuffle=False)
print(" Build data shape: " + str(build.shape) +  " Validation data shape: " + str(val.shape) + " and Testing data shape: " + str(test.shape))


rf = RandomForestRegressor(n_estimators=100, n_jobs=4, random_state=2018)
rf.fit(build[cols_for_model], y_build)


val_preds = rf.predict(val[cols_for_model])
test_preds = rf.predict(test[cols_for_model])
print("Validation RMSE:" +  str(mean_squared_error(y_val, val_preds)**0.5))
print("Test RMSE: " + str(mean_squared_error(y_test, test_preds)**0.5))
if write_data_flag:




plt.figure(figsize=(20,8))
plt.plot(load_data[["Time", "Load"]].set_index("Time"), label="Actual load")
plt.plot(pd.concat((val["Time"].reset_index(drop=True), pd.Series(val_preds, name="Load")), axis=1).set_index("Time"), color="red", label="Validation predictions")
plt.plot(pd.concat((test["Time"].reset_index(drop=True), pd.Series(test_preds, name="Load")), axis=1).set_index("Time"), color="blue", label="Test predictions")

plt.legend()


# ### 2. Predict the session length for a given IP

data_df = data.copy(deep=True)
cols = ["sent_bytes", "received_bytes", "response_processing_time"]
data_df[cols] = data_df[cols].astype(np.float)
data_df["Device"] = data_df["user_agent"].str.contains("Mobile").replace({False: "Web", True: "Mobile"})
data_df["request_method"] = data_df["request"].str.split(" ", expand=True)[0]




device_data = (data_df[["session_id", "Device"]].pivot_table(index="session_id", 
                                                    columns=["Device"], 
                                                    aggfunc=np.count_nonzero)
                                                .reset_index().rename_axis(None, axis=1).fillna(0))
session_details = data_df[["session_id"]+cols].pivot_table(index="session_id", values=cols).reset_index()


# #### Creating lag features based on session for each client

lag_cols = ["Mobile", "Web","received_bytes", "response_processing_time", "sent_bytes","session_length", "unique_url_count"]
tmp_0 = (pd.merge(session_length_data, url_count_data, how='left', on='session_id')
           .merge(device_data, on="session_id", how='left')
           .merge(session_details, on="session_id", how='left'))

tmp_1 = tmp_0.groupby("client_ip")[lag_cols].transform(lambda x: x.shift(1)).rename(columns=dict(zip(lag_cols, ["Prev_"+col for col in lag_cols])))
session_data = pd.concat((tmp_0.drop(np.setdiff1d(lag_cols, ["session_length", "unique_url_count"]), axis=1), 
                          tmp_1), axis=1)
session_data.head()



# Data is split based on session
# First 5 sessions in train
build = session_data[session_data["session"] < 6].fillna(0)
val = session_data[session_data["session"] == 6].fillna(0)
test = session_data[session_data["session"] > 6].fillna(0)


cols_for_model = ['Prev_Mobile', 'Prev_Web',
                   'Prev_received_bytes', 'Prev_response_processing_time',
                   'Prev_sent_bytes', 'Prev_session_length', 'Prev_unique_url_count']




rf_session_length = RandomForestRegressor(n_estimators=500, n_jobs=4, random_state=2018)
rf_session_length.fit(build[cols_for_model], np.log1p(build["session_length"]))


val_session_length_preds = rf_session_length.predict(val[cols_for_model])
test_session_length_preds = rf_session_length.predict(test[cols_for_model])
print("Validation RMSE: " + str(mean_squared_error(val['session_length'],np.expm1(val_session_length_preds))**0.5))
print("Test RMSE: " + str(mean_squared_error(test['session_length'], np.expm1(test_session_length_preds))**0.5))


# ### 3. Predict the number of unique URL visits by a given IP


rf_url_count = RandomForestRegressor(n_estimators=500, n_jobs=4, random_state=2018)
rf_url_count.fit(build[cols_for_model], build["unique_url_count"])



val_url_count_preds = rf_url_count.predict(val[cols_for_model])
test_url_count_preds = rf_url_count.predict(test[cols_for_model])
print("Validation RMSE: " + str(mean_squared_error(val['unique_url_count'], val_url_count_preds)**0.5))
print("Test RMSE: " + str(mean_squared_error(test['unique_url_count'], test_url_count_preds)**0.5))



test_df = test[["client_ip", "session", "session_length", "unique_url_count"]].copy()
test_df["session_length_prediction"] = np.expm1(test_session_length_preds)
test_df["unique_url_count_prediction"] = test_url_count_preds
if write_data_flag:
    test_df.to_csv(OUTPUT_PATH+ "section2_2-3_predictions.csv")





